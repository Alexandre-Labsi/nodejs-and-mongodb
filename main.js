//require express
const express = require('express');
const app = express();
const cors = require('cors');
const bson = require('bson');



//require mongo
const MongoClient = require('mongodb').MongoClient;
const { ReplSet } = require('mongodb');

//initialize connection
const url = 'mongodb://172.17.0.1:1221';
const dbName = 'mongoo';
let db;

//test connection 
MongoClient.connect(url, function(err, client) {
    if (err) throw err;
    console.log("Connected");
    db = client.db(dbName);
});

//middleware en params
app.use(express.urlencoded({extended: true})); 
app.use(express.json());

//middleware cors
app.use(cors());

/**
 * Retourne une liste de messages
 */
app.get('/messages', (req, res) => {
  //mongo collection
    db.collection('message').find().toArray(function(err, result) {
        if (err) {
            console.log(err)
            res.status(404)
            throw err
        }
        res.status(200);
        res.send(result);
    })
})

/**
 * Crée un nouveaux message
 */
app.post('/messages',(req, res) => {
    db.collection('message').insertOne(req.body, (err)=>{
        if(err){
            console.log(err)
            throw err
        }
        res.status(200)
        res.send()  
    })
})


/**
 * Passage a true si le message est lu
 */
app.put('/messages/:id/read', (req, res)=> {
    let base = {_id:bson.ObjectID(req.params.id)}
    let query = { $set: {read: true } };
    db.collection("message").updateOne(base, query,(err)=>{
        if (err){
            console.log(err)
            res.status(403)
            throw err;
        }
        res.status(200);
        res.send("Message updated");
    });
})

/**
 * Supprime un message
 */
app.delete('/messages/:id/delete', (req, res)=>{
    let id = {_id:bson.ObjectID(req.params.id)}
    db.collection("message").deleteOne(id,(err)=> {
        if (err){
            console.log(err);
            res.status(403)
            throw err;
        }
        res.status(200);
        res.send("Message removed")
    })
}) 



//Port par defaut NodeJS
app.listen(8081, _=> console.log('started') );